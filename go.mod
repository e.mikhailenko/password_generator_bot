module passwordGeneratorBot

go 1.13

require (
	github.com/joho/godotenv v1.4.0
	github.com/sethvargo/go-password v0.2.0
	gopkg.in/tucnak/telebot.v3 v3.0.0-20220116225720-981708682707
)
