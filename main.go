package main

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/sethvargo/go-password/password"
	"gopkg.in/tucnak/telebot.v3"
	"os"
	"strconv"
	"time"
)

type Text struct {
	Start               string
	AllowRepeat         string
	NoUpper             string
	EnterLength         string
	NumDigits           string
	NumSymbols          string
	Next                string
	BotRestarting       string
	ErrorPasswordLength string
}

type Config struct {
	AllowRepeat    bool
	NoUpper        bool
	PasswordLength int
	NumDigits      int
	NumSymbols     int
}

func init() {
	if err := godotenv.Load(); err != nil {
		fmt.Errorf("no .env file found")
	}
}

func readJSON() (text *Text, config *Config) {
	fileText, _ := os.Open("text.json")
	decoderText := json.NewDecoder(fileText)
	text = new(Text)
	err := decoderText.Decode(&text)

	defer func(fileText *os.File) {
		err := fileText.Close()
		if err != nil {
			fmt.Errorf(err.Error())
		}
	}(fileText)

	if err != nil {
		fmt.Errorf(err.Error())
	}

	fileConfig, _ := os.Open("config.json")
	decoderConfig := json.NewDecoder(fileConfig)
	config = new(Config)
	errConfig := decoderConfig.Decode(&config)

	defer func(fileConfig *os.File) {
		err := fileConfig.Close()
		if err != nil {
			fmt.Errorf(err.Error())
		}
	}(fileConfig)

	if errConfig != nil {
		//log.Fatal(errConfig)
		fmt.Errorf(errConfig.Error())
	}

	return
}

func main() {
	text, config := readJSON()

	bot, err := telebot.NewBot(telebot.Settings{
		Token:  os.Getenv("SECRET_TOKEN"),
		Poller: &telebot.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		//log.Fatal(err)
		fmt.Errorf(err.Error())
	}

	var (
		//menu                   = &telebot.ReplyMarkup{ResizeKeyboard: true}
		selectorNoUpper        = &telebot.ReplyMarkup{}
		selectorPasswordLength = &telebot.ReplyMarkup{}
		selectorNumDigits      = &telebot.ReplyMarkup{}
		selectorNumSymbols     = &telebot.ReplyMarkup{}

		btnYes = selectorNoUpper.Data("✅ Да", "btnYes")
		btnNo  = selectorNoUpper.Data("❌ Нет", "btnNo")

		btnPasswordLength10 = selectorPasswordLength.Data("10", "btnPasswordLength10")
		btnPasswordLength20 = selectorPasswordLength.Data("20", "btnPasswordLength20")
		btnPasswordLength30 = selectorPasswordLength.Data("30", "btnPasswordLength30")

		btnNumDigits0  = selectorNumDigits.Data("0", "btnNumDigits0")
		btnNumDigits5  = selectorNumDigits.Data("5", "btnNumDigits5")
		btnNumDigits10 = selectorNumDigits.Data("10", "btnNumDigits10")
		btnNumDigits15 = selectorNumDigits.Data("15", "btnNumDigits15")
		btnNumDigits20 = selectorNumDigits.Data("20", "btnNumDigits20")
		btnNumDigits25 = selectorNumDigits.Data("25", "btnNumDigits25")
		btnNumDigits30 = selectorNumDigits.Data("30", "btnNumDigits30")

		btnNumSymbols0  = selectorNumSymbols.Data("0", "btnNumSymbols0")
		btnNumSymbols5  = selectorNumSymbols.Data("5", "btnNumSymbols5")
		btnNumSymbols10 = selectorNumSymbols.Data("10", "btnNumSymbols10")
		btnNumSymbols15 = selectorNumSymbols.Data("15", "btnNumSymbols15")
		btnNumSymbols20 = selectorNumSymbols.Data("20", "btnNumSymbols20")
		btnNumSymbols25 = selectorNumSymbols.Data("25", "btnNumSymbols25")
		btnNumSymbols30 = selectorNumSymbols.Data("30", "btnNumSymbols30")
	)

	selectorNoUpper.Inline(
		selectorNoUpper.Row(btnYes, btnNo),
	)
	selectorPasswordLength.Inline(
		selectorPasswordLength.Row(btnPasswordLength10, btnPasswordLength20, btnPasswordLength30),
	)

	bot.Handle("/start", func(context telebot.Context) error {
		config.PasswordLength = 20
		config.NumDigits = 5
		config.NumSymbols = 5
		config.NoUpper = true

		return context.Send(text.Start+"\n\n"+text.EnterLength, selectorPasswordLength)
	})

	bot.Handle(&btnYes, func(context telebot.Context) error {
		config.NoUpper = false
		return context.Send(text.EnterLength, selectorPasswordLength)
	})

	bot.Handle(&btnNo, func(context telebot.Context) error {
		config.NoUpper = true
		return context.Send(text.EnterLength, selectorPasswordLength)
	})

	bot.Handle(&btnPasswordLength10, func(context telebot.Context) error {
		config.PasswordLength = 10
		selectorNumDigits.Inline(
			selectorNumDigits.Row(btnNumDigits0, btnNumDigits5, btnNumDigits10),
		)
		return context.Send(text.NumDigits, selectorNumDigits)
	})

	bot.Handle(&btnPasswordLength20, func(context telebot.Context) error {
		config.PasswordLength = 20
		selectorNumDigits.Inline(
			selectorNumDigits.Row(btnNumDigits0, btnNumDigits5, btnNumDigits10, btnNumDigits15, btnNumDigits20),
		)
		return context.Send(text.NumDigits, selectorNumDigits)
	})

	bot.Handle(&btnPasswordLength30, func(context telebot.Context) error {
		config.PasswordLength = 30
		selectorNumDigits.Inline(
			selectorNumDigits.Row(btnNumDigits0, btnNumDigits5, btnNumDigits10, btnNumDigits15, btnNumDigits20, btnNumDigits25, btnNumDigits30),
		)
		return context.Send(text.NumDigits, selectorNumDigits)
	})

	bot.Handle(&btnNumDigits0, func(context telebot.Context) error {
		config.NumDigits = 0
		switch config.PasswordLength {
		case 10:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10),
			)
		case 20:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10, btnNumSymbols15, btnNumSymbols20),
			)
		case 30:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10, btnNumSymbols15, btnNumSymbols20, btnNumSymbols25, btnNumSymbols30),
			)
		}
		return context.Send(text.NumSymbols, selectorNumSymbols)
	})

	bot.Handle(&btnNumDigits5, func(context telebot.Context) error {
		config.NumDigits = 5
		switch config.PasswordLength {
		case 10:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5),
			)
		case 20:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10, btnNumSymbols15),
			)
		case 30:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10, btnNumSymbols15, btnNumSymbols20, btnNumSymbols25),
			)
		}
		return context.Send(text.NumSymbols, selectorNumSymbols)
	})

	bot.Handle(&btnNumDigits10, func(context telebot.Context) error {
		config.NumDigits = 10
		switch config.PasswordLength {
		case 10:
			return context.Send(text.NoUpper, selectorNoUpper)
		case 20:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10),
			)
		case 30:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10, btnNumSymbols15, btnNumSymbols20),
			)
		}
		return context.Send(text.NumSymbols, selectorNumSymbols)
	})

	bot.Handle(&btnNumDigits15, func(context telebot.Context) error {
		config.NumDigits = 15
		switch config.PasswordLength {
		case 20:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5),
			)
		case 30:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10, btnNumSymbols15),
			)
		}
		return context.Send(text.NumSymbols, selectorNumSymbols)
	})

	bot.Handle(&btnNumDigits20, func(context telebot.Context) error {
		config.NumDigits = 20
		switch config.PasswordLength {
		case 20:
			return context.Send(text.NoUpper, selectorNoUpper)
		case 30:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5, btnNumSymbols10),
			)
		}
		return context.Send(text.NumSymbols, selectorNumSymbols)
	})

	bot.Handle(&btnNumDigits25, func(context telebot.Context) error {
		config.NumDigits = 25
		switch config.PasswordLength {
		case 30:
			selectorNumSymbols.Inline(
				selectorNumSymbols.Row(btnNumSymbols0, btnNumSymbols5),
			)
		}
		return context.Send(text.NumSymbols, selectorNumSymbols)
	})

	bot.Handle(&btnNumDigits30, func(context telebot.Context) error {
		config.NumDigits = 30
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols0, func(context telebot.Context) error {
		config.NumSymbols = 0
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols5, func(context telebot.Context) error {
		config.NumSymbols = 5
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols10, func(context telebot.Context) error {
		config.NumSymbols = 10
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols15, func(context telebot.Context) error {
		config.NumSymbols = 15
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols20, func(context telebot.Context) error {
		config.NumSymbols = 20
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols25, func(context telebot.Context) error {
		config.NumSymbols = 25
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnNumSymbols30, func(context telebot.Context) error {
		config.NumSymbols = 30
		return context.Send(text.NoUpper, selectorNoUpper)
	})

	bot.Handle(&btnYes, func(context telebot.Context) error {
		config.NoUpper = false
		result := ""
		for i := 1; i <= 10; i++ {
			passwordString, err := password.Generate(
				config.PasswordLength,
				config.NumDigits,
				config.NumSymbols,
				config.NoUpper,
				config.AllowRepeat,
			)
			if err != nil {
				fmt.Errorf(err.Error())
				return context.Send(text.ErrorPasswordLength + "\n\n" + text.BotRestarting)
			}
			result += strconv.Itoa(i) + ")	" + passwordString + "\n"
		}
		result += "\n" + text.BotRestarting
		return context.Send(result)
	})

	bot.Handle(&btnNo, func(context telebot.Context) error {
		config.NoUpper = false
		result := ""
		for i := 1; i <= 10; i++ {
			passwordString, err := password.Generate(
				config.PasswordLength,
				config.NumDigits,
				config.NumSymbols,
				config.NoUpper,
				config.AllowRepeat,
			)
			if err != nil {
				fmt.Errorf(err.Error())
				return context.Send(text.ErrorPasswordLength + "\n\n" + text.BotRestarting)
			}
			result += strconv.Itoa(i) + ")	" + passwordString + "\n"
		}
		result += "\n" + text.BotRestarting
		return context.Send(result)
	})

	bot.Handle("/generate", func(context telebot.Context) error {
		result, err := password.Generate(
			config.PasswordLength,
			config.NumDigits,
			config.NumSymbols,
			config.NoUpper,
			config.AllowRepeat,
		)
		if err != nil {
			fmt.Errorf(err.Error())
			//log.Fatal(err)
		}
		return context.Send(result)
	})

	bot.Start()
}
